/*
	MySQL Implementation of 2FA algorithm for use with Goole Authenticator app.
	Based on RFC 4226.

	(c) Hans Vanpée - 2017 - GPLv3

*/

/*
	A function that generates random 128 character keys
*/
DROP FUNCTION IF EXISTS g2fa_generate_key;
CREATE FUNCTION g2fa_generate_key()
RETURNS CHAR(128)
NOT DETERMINISTIC
COMMENT '2.0.0'

RETURN SHA2(CONVERT(RAND(), CHAR(128)), 512);

DELIMITER //

/*
	Convert a string to Base32
*/
DROP FUNCTION IF EXISTS g2fa_B32_encode //
CREATE FUNCTION g2fa_B32_encode(p_string VARBINARY(1024))
RETURNS VARBINARY(1024)
COMMENT '2.0.0'
BEGIN
	DECLARE str_len INT(10) UNSIGNED;
	DECLARE ind INT(10);
	DECLARE str_result VARBINARY(1024);
	DECLARE str_bin VARBINARY(1024);
	DECLARE b32charset BINARY(32);

	/* Initialize Base32 characterset string */
	SET b32charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';

	/* Convert p_string to binary string, 8 bits per character.
		 Use 64 bit blocks because CONV has max 64 bit precision.
	 */
	SET str_len = CHAR_LENGTH(p_string);
	SET ind = 1;
	SET str_bin = '';
	binloop: LOOP
		IF (str_len - ind + 1) >= 8 THEN
			SET str_bin = CONCAT(str_bin, LPAD(CONV(HEX(SUBSTR(p_string, ind, 8)),16,2), 64, '0'));
			SET ind = ind + 8;
		ELSE
			SET str_bin = CONCAT(str_bin, LPAD(CONV(HEX(SUBSTR(p_string, ind, str_len - ind + 1)),16,2), (str_len - ind + 1) * 8, '0'));
			SET ind = str_len + 1;
		END IF;

		IF ind > str_len THEN
			LEAVE binloop;
		END IF;
	END LOOP;

	/* Right-pad binary string to get multiple of 5 bits */
	SET str_len = CHAR_LENGTH(str_bin);
	SET str_bin = RPAD(str_bin, CEILING(str_len / 5) * 5, '0');

	/* Convert 5-bit sequences to Base32 characterset */
	SET str_len = CHAR_LENGTH(str_bin);
	SET ind = 1;
	SET str_result = '';
	resloop: LOOP
		SET str_result = CONCAT(str_result, SUBSTRING(b32charset,CONV(SUBSTRING(str_bin, ind, 5), 2, 10) + 1,1));
		SET ind = ind + 5;
		IF ind > str_len THEN
			LEAVE resloop;
		END IF;
	END LOOP resloop;

	/* pad result string with '=' if needed */
	SET str_len = CHAR_LENGTH(str_result);
	SET str_result = RPAD(str_result, CEILING(str_len * 5 / 40) * 8, '=');

	RETURN str_result;
END; //

/*
	Decode Base32 string, returns NULL when failed.
*/
DROP FUNCTION IF EXISTS `g2fa_B32_decode` //
CREATE FUNCTION `g2fa_B32_decode`(p_str_b32 VARCHAR(1024))
RETURNS VARBINARY(1024)
BEGIN
	DECLARE b32charset BINARY(32);
	DECLARE result VARBINARY(1024);
	DECLARE bitstring VARCHAR(5128);
	DECLARE ind INTEGER UNSIGNED;
	DECLARE str_len INTEGER UNSIGNED;
	DECLARE charpos INTEGER UNSIGNED;

	/* Initialize Base32 characterset string */
	SET b32charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
	SET p_str_b32 = UPPER(p_str_b32);

	/* Convert Base32 string to bitstring */
	SET ind = 1;
	SET bitstring = '';
	SET str_len = CHAR_LENGTH(p_str_b32);
	bitstring_loop: LOOP
		SET charpos = INSTR(b32charset, SUBSTR(p_str_b32, ind, 1));
		IF charpos IS NULL OR charpos = 0 THEN
			RETURN NULL;
		END IF;
		SET bitstring = CONCAT(bitstring, LPAD(RIGHT(CONV(charpos - 1, 10, 2),5),5,'0'));
		SET ind = ind + 1;
		IF ind > str_len OR SUBSTR(p_str_b32, ind, 1) = '=' THEN
			LEAVE bitstring_loop;
		END IF;
	END LOOP bitstring_loop;

	/* Trim trailing zeros after last 8-bit boundary */
	SET str_len = CHAR_LENGTH(bitstring);
	SET bitstring = LEFT(bitstring, 8 * FLOOR(str_len / 8));

	/* Convert bitstring to byte-based string */
	SET str_len = CHAR_LENGTH(bitstring);
	SET ind = 1;
	SET result = '';
	binloop: LOOP
		IF (str_len - ind + 1) > 64 THEN
			SET result = CONCAT(result, UNHEX(CONV(SUBSTR(bitstring, ind, 64), 2, 16)));
			SET ind = ind + 64;
		ELSE
			SET result = CONCAT(result, UNHEX(CONV(RPAD(SUBSTR(bitstring, ind, str_len - ind + 1), CEILING((str_len - ind + 1) / 8) * 8, '0'), 2, 16)));
			SET ind = str_len + 1;
		END IF;
		IF ind > str_len THEN
			LEAVE binloop;
		END IF;
	END LOOP;

	RETURN result;
END //

/*
	Generate 2FA secret key
*/
DROP FUNCTION IF EXISTS `g2fa_generate_key` //
CREATE FUNCTION `g2fa_generate_key`()
RETURNS VARCHAR(32)
NOT DETERMINISTIC
COMMENT '2.0.0'

BEGIN
	DECLARE char_key VARCHAR(24);
	SET char_key = LPAD(CONV(FLOOR(RAND() * POW(36,8)), 10, 36), 8, 0);
	SET char_key = CONCAT(char_key, LPAD(CONV(FLOOR(RAND() * POW(36,8)), 10, 36), 8, 0));
	SET char_key = CONCAT(char_key, LPAD(CONV(FLOOR(RAND() * POW(36,8)), 10, 36), 8, 0));
	SET char_key = SUBSTR(char_key, 1 + FLOOR(RAND() * 4), 20);

	RETURN g2fa_B32_encode(char_key);
END //

/*
	Calculate HMAC SHA1 hash for message and key
*/
DROP FUNCTION IF EXISTS `g2fa_hmac_sha1` //
CREATE FUNCTION `g2fa_hmac_sha1`(p_key VARBINARY(512), p_message VARBINARY(512))
RETURNS BINARY(20)
BEGIN
	DECLARE binkey BINARY(64);
	DECLARE okeypad VARBINARY(64);
	DECLARE ikeypad VARBINARY(64);
	DECLARE ind INTEGER;

	/* Hash keys longer than 64 characters, shorter strings are padded to 64 wide automatically */
	IF CHAR_LENGTH(p_key) > 64 THEN
		SET p_key = SHA1(p_key);
	END IF;

	/* Convert key to binary string */
	SET binkey = CONVERT(p_key, BINARY(64));

	SET ind = 1;
	SET okeypad = '';
	SET ikeypad = '';
	xorloop: LOOP
		SET okeypad = CONCAT(okeypad, CHAR(ASCII(SUBSTR(binkey, ind, 1)) ^ X'5c'));
		SET ikeypad = CONCAT(ikeypad, CHAR(ASCII(SUBSTR(binkey, ind, 1)) ^ X'36'));
		SET ind = ind + 1;
		IF ind > 64 THEN
			LEAVE xorloop;
		END IF;
	END LOOP xorloop;

	RETURN UNHEX(SHA1(CONCAT(okeypad, UNHEX(SHA1(CONCAT(ikeypad, p_message))))));
END //

/*
	Get 6 digit 2FA code for given key and message.
	p_2fa_key is a 32 character Base32 encoded string.
	p_offset specifies an offset from the current time slice.
	This function is based RFC 4226.
*/
DROP FUNCTION IF EXISTS `g2fa_get_code` //
CREATE FUNCTION `g2fa_get_code`(
	p_2fa_key BINARY(32),
	p_message BINARY(8)
)
RETURNS CHAR(6)
BEGIN
	DECLARE ind INTEGER(10) UNSIGNED;
	DECLARE hash2fa VARBINARY(20);
	DECLARE offset2fa TINYINT;
	DECLARE truncatedHash2fa BINARY(4);
	DECLARE code2fa BINARY(4);
	DECLARE code2faval INTEGER UNSIGNED;

	/* Get hash */
	SET hash2fa = g2fa_hmac_sha1(g2fa_B32_decode(p_2fa_key), p_message);
	/* Get lowest 4 bits from hash and use as index. MySQL strings start at index 1 */
	SET offset2fa = CONV(RIGHT(HEX(hash2fa), 1), 16, 10) + 1;
	SET truncatedHash2fa = SUBSTR(hash2fa, offset2fa, 4);
	/* Get 31 least significant bits */
	SET truncatedHash2fa = UNHEX(CONV(LPAD(RIGHT(CONV(HEX(truncatedHash2fa), 16, 2), 31), 32, '0'), 2, 16));

	SET code2faval = CONV(HEX(truncatedHash2fa), 16, 10);
	RETURN LPAD(CONVERT(code2faval % 1000000, CHAR), 6, '0');
END //

DROP FUNCTION IF EXISTS `g2fa_check_code` //
/* Check code p_2fa_code against key p_2fa_key and allow p_steps_before steps back and p_steps_after steps forward.
	 Returns TRUE if code is valid, FALSE if not.
 */
CREATE FUNCTION `g2fa_check_code`(
	p_2fa_key BINARY(32),
	p_steps_before INTEGER UNSIGNED,
	p_steps_after INTEGER UNSIGNED,
	p_2fa_code CHAR(6)
)
RETURNS BOOLEAN
BEGIN
	DECLARE half_min_ts BIGINT;
	DECLARE ind INTEGER;

	/* Validate input */
	IF p_2fa_key IS NULL OR p_2fa_code IS NULL THEN
		RETURN FALSE;
	END IF;

	/* Get current timestamp in half minutes */
	SET half_min_ts = FLOOR(UNIX_TIMESTAMP() / 30);

	/* Check exact timeslice */
	IF g2fa_get_code(p_2fa_key, UNHEX(LPAD(HEX(half_min_ts), 16, '0'))) = p_2fa_code THEN
		RETURN TRUE;
	END IF;

	/* Verify if backward steps are to be checked, hardcoded upper limit is 10 steps */
	IF p_steps_before IS NOT NULL AND p_steps_before > 0 AND p_steps_before < 11 THEN
		SET ind = 1;
		back_loop: LOOP
			IF g2fa_get_code(p_2fa_key, UNHEX(LPAD(HEX(half_min_ts - ind), 16, '0'))) = p_2fa_code THEN
				RETURN TRUE;
			END IF;
			SET ind = ind + 1;
			IF ind > p_steps_before THEN
				LEAVE back_loop;
			END IF;
		END LOOP back_loop;
	END IF;

	/* Verify if forward steps are to be checked, hardcoded upper limit is 10 steps */
	IF p_steps_after IS NOT NULL AND p_steps_after > 0 AND p_steps_after < 11 THEN
		SET ind = 1;
		forward_loop: LOOP
			IF g2fa_get_code(p_2fa_key, UNHEX(LPAD(HEX(half_min_ts + ind), 16, '0'))) = p_2fa_code THEN
				RETURN TRUE;
			END IF;
			SET ind = ind + 1;
			IF ind > p_steps_after THEN
				LEAVE forward_loop;
			END IF;
		END LOOP forward_loop;
	END IF;

	RETURN FALSE;
END //
