MySQL 2FA implementation
========================

This is an RFC 4226 based two factor authentication written for MySQL.
It was tested on MySQL 5.5.55.

Usage
-----

- Function *g2fa_generate_key* can be used to gererate a new secret key. It takes no parameters and it returns a 32 character Base32 encoded value.
- Use function *g2fa_check_code* to verify if a given 6-digit code is valid for a secret key at the current timestamp. Code validity duration is 30 seconds. To give users more time to lookup and enter their code, the number of 30-second intervals before the current one that should be verified (p_steps_before) must be larger than 0. The number of intervals after the current one can be specified also to compensate for differences in system time between client and server. Keep these values low (less than 5). The function takes 4 parameters (the secret key, the number of intervals before the current one, the number after current and the 6-digit code supplied by the user). It returns TRUE if the code was valid, FALSE if not.

Background
----------

I wrote these functions for a project where I needed RFC 4226 based 2FA authentication. To reduce the risk of secret keys being retrieved by malicious server-side scripts, the functions had to be fully implemented in MySQL. The only time a secret key is sent back to a script is when creating a new key. All interactions with the database are handled by stored procedures and none of these returns a user's secret key, save for the one that creates a new key.
